/// <reference path="../definitions/definitions" />

import ws = require("ws")

/**
 * რეალური პაკეტი რაც იგზავნება ქსელში
 */
interface RPCPacket {
    func: string;
    params: Object;
}

export interface RPCCallback {
    (params: Object, sender:RPCClient): void;
}

export class RPCClient {
    private socket: ws

    constructor(client: ws) {
        this.socket = client
    }

    callRemoteProcedure(func: string, params: Object) {
        var msgString = JSON.stringify(<RPCPacket>{
             func: func,
             params: params
        })
        
        this.socket.send(msgString)
    }

    getSocket(): ws {
        return this.socket
    }
}
 
/**
 * RPC-ი WebSocket-ის ბაზაზე, სერვერი
 */
export class RPCWSServer {
    private rpcClients: Array<RPCClient>
    private wss: ws.Server
    private rpcCallbacks: {[index:string]:RPCCallback}
    
    /**
     * სტარტავს RPC სერვერს მითითებულ პორტზე, websocket-ით
     */
    constructor(port: number) {

        this.rpcCallbacks = {}
        this.rpcClients = new Array()

        this.wss = ws.createServer
            ({
                port: port
            },
            (client) => {
                this.webSocketClientConnected(client)
            })

    }

    private addClient(rpcClient: RPCClient): void {
        var availableSlot = -1

        for (var slotIndex = 0; this.rpcClients.length; slotIndex++) {
            if (this.rpcClients[slotIndex] == null) {
                availableSlot = slotIndex
                this.rpcClients[slotIndex] = rpcClient
                break
            }
        }
        
        //თავისუფალი სლოტის არ არსებობსი შემთხვევაში ახალ ელემენტად
        //ვამატებ მასივში
        if (availableSlot == -1) {
            this.rpcClients.push(rpcClient)
        }
    }

    private removeClient(rpcClient: RPCClient): void {
        for (var slotIndex = 0; slotIndex < this.rpcClients.length; slotIndex++) {
            if (this.rpcClients[slotIndex] == rpcClient) {
                this.rpcClients[slotIndex] = null
                break
            }
        }
    }

    private webSocketMessage(client: RPCClient, data: any, flags: any) {
        var msg = <RPCPacket>JSON.parse(data)
        
        try
        {
            this.rpcCallbacks[msg.func](msg.params, client)
        }catch(e){}     
    }

    private webSocketClose(client: RPCClient, code: number, message: string) {

        this.removeClient(client)

    }

    private webSocketClientConnected(client: ws) {

        var rpcClient = new RPCClient(client)

        this.addClient(rpcClient)

        client.on(
            "message",
            (data: any, flags: any) => {
                var _rpcClient = rpcClient
                this.webSocketMessage(_rpcClient, data, flags)
            }
        )

        client.on(
            "close",
            (code: number, message: string) => {
                var _rpcClient = rpcClient
                this.webSocketClose(_rpcClient, code, message)
            }
        )
    }
    
    /**
     * ამატებს კლიენტის მიერ გამოძახებად ფუნქციას, მითითებული სახელით.
     */
    setCallback(callbackName: string, callback?: RPCCallback) {
        this.rpcCallbacks[callbackName] = callback
    }
    
} 

export function createRPCServer(port:number):RPCWSServer
{
    return new RPCWSServer(port)
}