/// <reference path="../definitions/definitions" />
var ws = require("ws");
var RPCClient = (function () {
    function RPCClient(client) {
        this.socket = client;
    }
    RPCClient.prototype.callRemoteProcedure = function (func, params) {
        var msgString = JSON.stringify({
            func: func,
            params: params
        });
        this.socket.send(msgString);
    };
    RPCClient.prototype.getSocket = function () {
        return this.socket;
    };
    return RPCClient;
})();
exports.RPCClient = RPCClient;
/**
 * RPC-ი WebSocket-ის ბაზაზე, სერვერი
 */
var RPCWSServer = (function () {
    /**
     * სტარტავს RPC სერვერს მითითებულ პორტზე, websocket-ით
     */
    function RPCWSServer(port) {
        var _this = this;
        this.rpcCallbacks = {};
        this.rpcClients = new Array();
        this.wss = ws.createServer({
            port: port
        }, function (client) {
            _this.webSocketClientConnected(client);
        });
    }
    RPCWSServer.prototype.addClient = function (rpcClient) {
        var availableSlot = -1;
        for (var slotIndex = 0; this.rpcClients.length; slotIndex++) {
            if (this.rpcClients[slotIndex] == null) {
                availableSlot = slotIndex;
                this.rpcClients[slotIndex] = rpcClient;
                break;
            }
        }
        //თავისუფალი სლოტის არ არსებობსი შემთხვევაში ახალ ელემენტად
        //ვამატებ მასივში
        if (availableSlot == -1) {
            this.rpcClients.push(rpcClient);
        }
    };
    RPCWSServer.prototype.removeClient = function (rpcClient) {
        for (var slotIndex = 0; slotIndex < this.rpcClients.length; slotIndex++) {
            if (this.rpcClients[slotIndex] == rpcClient) {
                this.rpcClients[slotIndex] = null;
                break;
            }
        }
    };
    RPCWSServer.prototype.webSocketMessage = function (client, data, flags) {
        var msg = JSON.parse(data);
        try {
            this.rpcCallbacks[msg.func](msg.params, client);
        }
        catch (e) { }
    };
    RPCWSServer.prototype.webSocketClose = function (client, code, message) {
        this.removeClient(client);
    };
    RPCWSServer.prototype.webSocketClientConnected = function (client) {
        var _this = this;
        var rpcClient = new RPCClient(client);
        this.addClient(rpcClient);
        client.on("message", function (data, flags) {
            var _rpcClient = rpcClient;
            _this.webSocketMessage(_rpcClient, data, flags);
        });
        client.on("close", function (code, message) {
            var _rpcClient = rpcClient;
            _this.webSocketClose(_rpcClient, code, message);
        });
    };
    /**
     * ამატებს კლიენტის მიერ გამოძახებად ფუნქციას, მითითებული სახელით.
     */
    RPCWSServer.prototype.setCallback = function (callbackName, callback) {
        this.rpcCallbacks[callbackName] = callback;
    };
    return RPCWSServer;
})();
exports.RPCWSServer = RPCWSServer;
function createRPCServer(port) {
    return new RPCWSServer(port);
}
exports.createRPCServer = createRPCServer;
